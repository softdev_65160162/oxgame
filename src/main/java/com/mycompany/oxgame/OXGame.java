/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxgame;

import java.util.Scanner;

/**
 *
 * @author MisterNrazZ
 */
public class OXGame {

    static char[][] table = {{'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}};
    static char player = 'O';
    static int num;
    static char numString;
    static int count=0;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputNum();
            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
        
    }

    private static void printWelcome() {
        System.out.println("Welcome OX");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void printTurn() {
        System.out.println(player + " Turn");
    }

    private static void inputNum() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input number:");
        num = kb.nextInt();
        numString = (char) num;
        if (num <= 9) {
            if (num == 1) {
                table[0][0] = player;
            } else if (num == 2) {
                table[0][1] = player;
            } else if (num == 3) {
                table[0][2] = player;
            } else if (num == 4) {
                table[1][0] = player;
            } else if (num == 5) {
                table[1][1] = player;
            } else if (num == 6) {
                table[1][2] = player;
            } else if (num == 7) {
                table[2][0] = player;
            } else if (num == 8) {
                table[2][1] = player;
            } else if (num == 9) {
                table[2][2] = player;
            }
            count++;
        }
    }

    private static void switchPlayer() {
        if (player == 'O') {
            player = 'X';
        } else {
            player = 'O';
        }
    }

    private static boolean isWin() {
        if (check()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(player + " Win");
    }

    private static boolean check() {
        if (table[0][0] == table[1][1] && table[0][0] == table[2][2]) {
            return true;
        } else if (table[0][2] == table[1][1] && table[0][2] == table[2][0]) {
            return true;
        } else if (table[0][0] == table[0][1] && table[0][0] == table[0][2]) {
            return true;
        } else if (table[1][0] == table[1][1] && table[1][0] == table[1][2]) {
            return true;
        } else if (table[2][0] == table[2][1] && table[2][0] == table[2][2]) {
            return true;
        } else if (table[0][0] == table[1][0] && table[0][0] == table[2][0]) {
            return true;
        } else if (table[0][1] == table[1][1] && table[0][1] == table[2][1]) {
            return true;
        } else if (table[0][2] == table[1][2] && table[0][2] == table[2][2]) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        if(count==9){
            return true;
        }
        return false;

    }

    private static void printDraw() {
        System.out.print("Dawn");
    }
}
